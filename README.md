# tf-aws-iam-policy-change-password

========================

Creates IAM policy change password users.

## Usage

```hcl-terraform

module "iam_policy_change_password" {

  source = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/iam/tf-aws-iam-policy-change-password.git?ref=tags/0.0.1"
  #source = "./modules/iam_policy_change_password"


  create = true

  name = "${var.cut_name}-ChangePasswordPolicy"
  
  description = "This policy allows users to change their own passwords."
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-------:|:--------:|
| create | The Whether of the policy. | bool | `false` | no |
| name | The Name of the policy. | string | null | yes |
| description | The Description of the policy. | string | null | yes |

## Outputs

| Name | Description |
|------|-------------|
| iam_policy_id | The policy's ID |
| iam_policy_arn | The ARN assigned by AWS to this policy |
| iam_policy_name | The name of the policy |
