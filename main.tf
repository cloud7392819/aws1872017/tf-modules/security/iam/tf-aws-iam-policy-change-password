# ws@2023 main.tf

resource "aws_iam_policy" "default" {

  count = local.create ? 1 : 0

  name = local.name

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with name.
  #name_prefix = "${local.name}-"

  # (Optional, Forces new resource) Description of the IAM policy.
  description = var.description

  # (Required) The policy document. This is a JSON formatted string.
  policy      = join("", data.aws_iam_policy_document.default.*.json)
}

