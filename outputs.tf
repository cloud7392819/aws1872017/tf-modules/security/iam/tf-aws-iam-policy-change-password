# ws@2023 outputs.tf

output "iam_policy_id" {
  description = "The policy's ID"
  value       = join("", aws_iam_policy.default.*.id)
}

output "iam_policy_arn" {
  description = "The ARN assigned by AWS to this policy"
  value       = join("", aws_iam_policy.default.*.arn)
}

output "iam_policy_name" {
  description = "The name of the policy"
  value       = join("", aws_iam_policy.default.*.name)
}
