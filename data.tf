# ws@2023 data.tf

# See:
# * https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_aws_my-sec-creds-self-manage.html

data "aws_iam_policy_document" "default" {

  count = local.create ? 1 : 0

  statement {

    sid = "ChangePassword"

    effect = "Allow"

    actions = [
      "iam:ChangePassword"
    ]

    resources = [
      "arn:aws:iam::*:user/$${aws:username}"
    ]
  }

  statement {

    sid = "GetAccountPasswordPolicy"

    effect = "Allow"

    actions = [
      "iam:GetAccountPasswordPolicy"
    ]

    resources = [
      "*"
    ]
  }  
}


